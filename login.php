<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Collin Svenby 9/5/17 -->

    <title>**REAL SIGHTINGS**</title>

    <link rel="stylesheet" type="text/css" href="css/style.css">

    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" type="text/css" href="lightbox.css">

    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/lightbox.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $('a').smoothScroll({
                speed: 1000,
                easing: 'easeInOutCubic'
            });
            $('.showOlderChanges').on('click', function (e) {
                $('.changelog .old').slideDown('slow');
                $(this).fadeOut();
                e.preventDefault();
            })
        });
    </script>
			<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script type="text/JavaScript">

var picCount=0; // global
var picArray= ["images/smoogle.png","images/nilbog.png","images/pokeoAdd.png","images/McDonald'sAD.png","images/callToWar.png", ]
//
// gets next picture in array
function nextPic()
{ // check if adding 1 exceeds number of pics in array
picCount=(picCount+1<picArray.length)? picCount+1 : 0;
// build the img to write to page using the new pic reference
var build='<img border="0" src="'+picArray[picCount]+'" width="260" height="100%">\
';
document.getElementById("imgHolder").innerHTML=build;
// repeat this after a pause of 8000ms (8sec).
setTimeout('nextPic()',8000)
}

</script>
</head>

<body onload="setTimeout('nextPic()',8000)">

<div id="container">

		<header>
			<a href="index.php">
			<img src="images/alienbanner2.png">	
			</a>
		</header>

		<nav class="sticky">
				<ul>
					<li><a href="index.php" title="Home">Home</a></li>
					<li><a href="about.html" title="About">About Us</a></li>
					<li><a href="tips.html" title="Tips">Donate</a></li>
					<li><a href="photos.html" title="Photos">Photos</a></li>
					<li><a href="login.php">
					<button type="submit" class="submit" id="submit" name="Submit" value="Login" action="login.php"/><span>Login </span></button>
					</a></li>
				</ul>
		</nav>
			<div id="ad" class="sticky">
					<h3>Sponsored by:</h3>	
						<div id="imgHolder">
							<img border="0" src="img1.gif" width="200" height="1000">
						</div>
			</div><!-- End ad -->
    <div id="content">

        <div id="colTwo">
                <h1>Log in!</h1>
                <div id="contact_form">
                    <form name="contact" method="post" action="*">
                        <fieldset>
                            <legend>Sign in Form</legend>
                            <?php
                            $msg = '';

                            if (isset($_POST['login']) && !empty($_POST['username'])
                                && !empty($_POST['password'])) {

                                if ($_POST['username'] == 'tutorialspoint' &&
                                    $_POST['password'] == '1234') {
                                    $_SESSION['valid'] = true;
                                    $_SESSION['timeout'] = time();
                                    $_SESSION['username'] = 'tutorialspoint';

                                    echo 'You have entered valid use name and password';
                                }else {
                                    $msg = 'Wrong username or password';
                                }
                            }
                            ?>
                            <form class = "form-signin" role = "form"
                                  action = "<?php echo htmlspecialchars($_SERVER['PHP_SELF']);
                                  ?>" method = "post">
                                <h4 class = "form-signin-heading"><?php echo $msg; ?></h4>
                                <input type = "text" class = "form-control"
                                       name = "username" placeholder = "username"
                                       required autofocus></br>
                                <input type = "password" class = "form-control"
                                       name = "password" placeholder = "password" required>
                                <button class = "submit-2" type = "submit"
                                        name = "login"><span>Login </span></button>
                            </form>
							</br>
                            &nbsp Click here to <a href = "php/logout.php" tite = "Logout"><button type="submit" class="submit-2" name="Submit" value="Login" action="login.php"/><span>Logout </span></button>
					</a>
                        </fieldset>
                    </form>
                </div><!--end contact_form-->
            </div><!--end colTwo -->


    </div><!--end content -->

    </div><!--end content-->

		<footer>
			<div id="footer_nav">
				<table id="footer_table">
					<tr>
						<td>Designed By:</td>
						<td>Connor Overturf</td>
						<td>Michael Berryhill</td>
						<td>Collin Svenby</td>
						<td>Alsaddig Ibrahim</td>
					</tr>
					<tr>
						<td>&nbsp </td>
						<td>Hayden Hogue</td>
						<td>Travis Brunner</td>
						<td>Zach Garrett</td>
					</tr>
				</table>
			</div><!--end footer nav-->
			<div id="footerCopyright">
				REAL SIGHTINGS INC.
			</div><!--end footerCopyright -->
		</footer>

</div><!--end container-->

</body>

</html>