<!DOCTYPE html>
<html lang="en">
<head>

<!-- Collin Svenby 9/5/17 -->

<title>**REAL SIGHTINGS**</title>

<link rel="stylesheet" type="text/css" href="css/style.css">

<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

<link rel="stylesheet" type="text/css" href="lightbox.css">

			<script src="js/jquery-1.7.2.min.js"></script>
			<script src="js/lightbox.js"></script>
			<script>
				 
				 
				 jQuery(document).ready(function($) {
					 $('a').smoothScroll({
						 speed: 1000,
						 easing: 'easeInOutCubic'
					 });
					 $('.showOlderChanges').on('click', function(e){
						 $('.changelog .old').slideDown('slow');
						 $(this).fadeOut();
						 e.preventDefault();
					 })
				 });

			</script>
			<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script type="text/JavaScript">

var picCount=0; // global
var picArray= ["images/smoogle.png","images/nilbog.png","images/pokeoAdd.png","images/McDonald'sAD.png","images/callToWar.png", ]
//
// gets next picture in array
function nextPic()
{ // check if adding 1 exceeds number of pics in array
picCount=(picCount+1<picArray.length)? picCount+1 : 0;
// build the img to write to page using the new pic reference
var build='<img border="0" src="'+picArray[picCount]+'" width="260" height="100%">\
';
document.getElementById("imgHolder").innerHTML=build;
// repeat this after a pause of 8000ms (8sec).
setTimeout('nextPic()',8000)
}

</script>
</head>

<body onload="setTimeout('nextPic()',8000)">


		<header>
			<a href="index.php">
			<img src="images/alienbanner2.png">	
			</a>
		</header>
		
		<nav class="sticky">
			<ul>
				<li><a href="index.php" title="Home">Home</a></li>
				<li><a href="about.html" title="About">About Us</a></li>
				<li><a href="tips.html" title="Tips">Donate</a></li>
				<li><a href="photos.html" title="Photos">Photos</a></li>
				<li><a href="login.php">
				<button type="submit" class="submit" id="submit" name="Submit" value="Login" action="login.php"/><span>Login </span></button></a></li>
                <li><a href="php/new_topic.php/">
                <button type="submit" class="submit" id="submit" name="Submit" value="Create New Topic" action="new_topic.php"/><span>Create New Topic </span></button></a></li>					
			</ul>
		</nav>

			<div id="ad" class="sticky">
					<h3>Sponsored by:</h3>	
						<div id="imgHolder">
							<img border="0" src="img1.gif" width="200" height="1000">
						</div>
			</div><!-- End ad -->
			<div id="content">

			<div id="colTwo">	
				<div class="post">
				<h1>Check out our threads!</h1>
                    <?php
                    $host="localhost"; // Host name
                    $username="root"; // Mysql username
                    $password=""; // Mysql password
                    $db_name="bytejam2017"; // Database name
                    $tbl_name="fquestions"; // Table name

                    // Connect to server and select databse.
                    $con=mysqli_connect("$host", "$username", "$password")or die("cannot connect");
                    mysqli_select_db($con,"$db_name")or die("cannot select DB");

                    $sql="SELECT * FROM $tbl_name ORDER BY id DESC";
                    // OREDER BY id DESC is order result by descending

                    $result=mysqli_query($con,$sql);
                    ?>

                    <table class="topic" width="90%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
                        <tr>
                            <td width="6%" align="center" bgcolor="#EEBFED"><strong>#</strong></td>
                            <td width="53%" align="center" bgcolor="#EEBFED"><strong>Topic</strong></td>
                            <td width="15%" align="center" bgcolor="#EEBFED"><strong>Views</strong></td>
                            <td width="13%" align="center" bgcolor="#EEBFED"><strong>Replies</strong></td>
                            <td width="13%" align="center" bgcolor="#EEBFED"><strong>Date/Time</strong></td>
                        </tr>

                        <?php

                        // Start looping table row
                        while($rows = mysqli_fetch_array($result)){
                            ?>
                            <tr>
                                <td bgcolor="#FFFFFF"><?php echo $rows['id']; ?></td>
                                <td bgcolor="#FFFFFF"><a href="php/view_topic.php?id=<?php echo $rows['id']; ?>"><?php echo $rows['topic']; ?></a><BR></td>
                                <td align="center" bgcolor="#FFFFFF"><?php echo $rows['view']; ?></td>
                                <td align="center" bgcolor="#FFFFFF"><?php echo $rows['reply']; ?></td>
                                <td align="center" bgcolor="#FFFFFF"><?php echo $rows['datetime']; ?></td>
                            </tr>

                            <?php
// Exit looping and close connection
                        }
                        mysqli_close($con);
                        ?>

                        <tr>
                            <td colspan="5" align="right" bgcolor="#EEBFED"><a href="php/new_topic.php"><strong>Create New Topic</strong> </a></td>
                        </tr>
                    </table>
					</br>
					</br>
				</div>
			</div><!--end colTwo -->
				
			</div><!--end content-->

		<footer>
			<div id="footer_nav">
				<table id="footer_table">
					<tr>
						<td>Designed By:</td>
						<td>Connor Overturf</td>
						<td>Michael Berryhill</td>
						<td>Collin Svenby</td>
						<td>Alsaddig Ibrahim</td>
					</tr>
					<tr>
						<td>&nbsp </td>
						<td>Hayden Hogue</td>
						<td>Travis Brunner</td>
						<td>Zach Garrett</td>
					</tr>
				</table>
			</div><!--end footer nav-->
			<div id="footerCopyright">
				REAL SIGHTINGS INC.
			</div><!--end footerCopyright -->
		</footer>
	
</body>

</html>